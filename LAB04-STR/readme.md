# Exercício 1
### Descrição do exercício
Neste exercício foi proposto a construção/codificação um arduino que irá, através de um sensor de luminosidade, emitir um som (Buzzer) se a medição do sensor for menor que a quantidade de luminosidade esperada. O sensor de luminosidade será contextualizado através da porta serial 'A0', o LED ativado pelo pino digital 7 e o buzzer ativado pelo pino digital número 8. 

### Comentários sobre o código fonte

Foram definidar 2 variáveis, analogPin contendo 'A0' e uma variável que irá armazenar os dados de luminosidade (val). Na função setup foram definidas os pinMode, dos valores de analogPin, 7 e 8. Por fim, na função de Loop, através da função analogRead será obtido o valor de luminosidade e adicionado na variável val, printamos isso e o LED é ligado (pin 7). E será feito um teste, por if e else, que caso val for menor que 600 o Buzzer será ativado, caso não ele será desativado. Foram usados nos valores de escrita do digitalWrite LOW e HIGH, somente.

#### OBS:
Na resolução do exercício foi usado a plataforma TinkerCad, por isso os valores de luminosidade foram emulados por uma interface disponibilizada pelo próprio TinkerCad, uma espécie de botão-interruptor e não pelo LED.

![](gifs/ex3.gif)