int analogPin = A0;
int val = 0;

void setup()
{
  Serial.begin(9600);
  pinMode(analogPin, INPUT);
  pinMode(8, OUTPUT);
  pinMode(7, OUTPUT);
}
void loop(){
  int val = analogRead(analogPin);
  Serial.println(val);
  digitalWrite(7,HIGH);
  if(val < 600){
    digitalWrite(8, HIGH);
  }
  else{
    digitalWrite(8, LOW);
  } 
  delay(1000);
}
