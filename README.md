Este repositório contém as resoluções dos exercícios presentes nas listas escolhidas para entrega do trabalho da matéria Sistema em Tempo Real e Embarcados, senda elas: 202002-Lista01-STR; 202002-Lab02-STR; 202002-Lab03-STR; 202002-Lab04-STR.\
Integrantes do Grupo são: Marcelo Alves, Taynara Souza e Vitor Maurício.

# Lista01-STR
https://gitlab.com/cLLProjects/stre-02-2020/-/tree/master/Lista01-STR

# LAB02-STR
https://gitlab.com/cLLProjects/stre-02-2020/-/tree/master/LAB02-STR 

# LAB03-STR
https://gitlab.com/cLLProjects/stre-02-2020/-/tree/master/LAB03-STR 

# LAB04-STR
https://gitlab.com/cLLProjects/stre-02-2020/-/tree/master/LAB04-STR 