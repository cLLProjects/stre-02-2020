# Lab03-STR
## Exercicio 1
### Descrição do exercício
Neste exercício proposto foi passado um código fonte .ino e, através da plataforma online TinkerCad, realizamos então a montagem do arduino.\
O exercício consiste em conectar um LED ao arduino e ligá-lo e desligá-lo através do código fonte.\
Primeiro conectamos o LED a um resistor e conectamos esse resistor à porta 13, após isso conectamos a outra "perna" do LED ao GND(Terra). 

### Comentários sobre o código fonte
No código fonte, realizamos primeiro a declaração e configuração do LED como saída na função setup(), a qual é chamada antes da execução da função loop().\
Na função loop, realizamos a "escrita digital" através da função digitalWrite(), passando os parametros necessários para execução, sendo eles o número do pino do Arduino e o valor HIGH para podemor acender o LED.\
Logo após é chamada a função delay(1000), o qual obriga o sistema esperar por 1000 ms antes de executar a próxima linha de código. Nesta próxima linha desligamos o LED através da função digitalWrite() novamente, mas no parametro de valor, foi passado LOW para desligar o LED, após isso foi chamado novamente a função delay(1000).\
Com este código podemos fazer com que o LED fique piscando conforme o gif a seguir.
![](gifs/ex1.gif)

## Exercicio 2
### Descrição do exercício
Neste exercício proposto foi passado um código fonte .ino e, através da plataforma online TinkerCad, realizamos então a montagem do arduino. \
O exercício consiste em conectar um botão ao arduino e, quando pressionado, é impresso, na saída do programa, o valor 1 e quando o botão não é pressionado é impresso o valor 0. Primeiro conectamos o botão a placa de circuito e então conectamos um cabo na porta 2 do arduino e na placa, paralelamente com o botão para conectá-los, é conectado também o botão a porta referente aos 5V do arduino. Após isso foi conectado um resitor que então é conectado ao Terra.

### Comentários sobre o código fonte
Na função setup() (executada antes da função loop()), inicializamos o Serial (Utilizado para fazer a comunicação entre o arduino e o computador, apenas para mostrar no monitor uma saída) através da chamada da função Serial.begin(9600), este parametro define a velocidade de transmissão de dados em bits por segundo. \
Depois declaramos e configuramos o nosso botão através da função pinMode() passando os parametros: 2 (sendo o pin do Arduino onde o botão estará conectado) e INPUT (para definir que o botão é do tipo entrada).\
Na função Loop() é realizado primeiro a leitura do estado do botão, para isso é armazenado numa variável do tipo inteiro o retorno da chamada da função digitalRead() passando como parametro a porta que o botão esta conectada ao arduino. Esta função retorna 0 se o botão não estiver sendo pressionado e 1 caso esteja sendo pressionado.\
Após isso é impresso na tela do usuário através da função Serial.println() (passando como parametro o valor presente na variável mencionada anteriormente), para que fique claro que o botão está ou não sendo pressionado. Após isso é chamada a função delay(1), mas esta não é muito útil, tendo em vista que está esperando apenas 1 ms. Depois do exposto o loop reinicia e é executado tudo novamente.\
Com este código podemos fazer com que possamos ver na tela o status do botão (está sendo pressionado ou não). 
![](gifs/ex2.gif)
